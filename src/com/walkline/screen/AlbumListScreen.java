package com.walkline.screen;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;

import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Album;
import com.walkline.music.inf.AlbumDetails;
import com.walkline.music.inf.Song;
import com.walkline.util.Enumerations.RefreshActions;
import com.walkline.util.ui.ForegroundManager;
import com.walkline.util.ui.ListStyleButtonField;
import com.walkline.util.ui.ListStyleButtonSet;

public final class AlbumListScreen extends MainScreen
{
	ForegroundManager _foreground = new ForegroundManager(0);
	ListStyleButtonSet _songsSet = new ListStyleButtonSet();
	ListStyleButtonField _item;
	MusicSDK _music;
	Album _album;

    public AlbumListScreen(MusicSDK music, Album album)
    {
    	super (NO_VERTICAL_SCROLL | USE_ALL_HEIGHT | NO_SYSTEM_MENU_ITEMS);

        setTitle("专辑列表" + " - " + album.getName());

        _music = music;
        _album = album;
        add(_foreground);

        UiApplication.getUiApplication().invokeLater(new Runnable()
        {
			public void run()
			{
				getAlbumList();
			}
		});
    }

	private void getAlbumList()
	{
		RefreshContentsScreen popupScreen = new RefreshContentsScreen(_music, String.valueOf(_album.getId()), RefreshActions.ALBUMDETAILS);
		UiApplication.getUiApplication().pushModalScreen(popupScreen);

		AlbumDetails albumDetails = popupScreen.getAlbumDetails();

		if (popupScreen != null) {popupScreen = null;}
		if (albumDetails != null) {refreshAlbumDetails(albumDetails);}
	}

	private void refreshAlbumDetails(AlbumDetails album)
	{
		Song[] songsList = album.getAlbum().getSongList();
		Song song;

		if (songsList.length <= 0) {return;}

		if (_songsSet.getManager() == null) {_foreground.add(_songsSet);}
		if (_songsSet.getFieldCount() > 0) {_songsSet.deleteAll();}

		for (int i=0; i<songsList.length; i++)
		{
			song = songsList[i];
			if (song != null)
			{
				_item = new ListStyleButtonField(song);
				_item.setChangeListener(new FieldChangeListener()
				{
					public void fieldChanged(Field field, int context)
					{
						if (context != FieldChangeListener.PROGRAMMATIC) {} //getSongDetails();}
					}
				});
				_songsSet.add(_item);
			}
		}
	}

	private void getSongDetails()
	{
		//UiApplication.getUiApplication().invokeLater(new Runnable()
    	//{
		//	public void run()
		//	{
				//ListStyleButtonField item = (ListStyleButtonField) _listSet.getFieldWithFocus();
				//RefreshContentsScreen popupScreen = new RefreshContentsScreen(_music, String.valueOf(item.getSongId()), RefreshActions.SONGDETAILS);
				//UiApplication.getUiApplication().pushModalScreen(popupScreen);

				//final SongDetails songDetails = popupScreen.getSongDetail();

				//if (popupScreen != null) {popupScreen = null;}
				//if (songDetails != null)
				//{
				//	UiApplication.getUiApplication().invokeLater(new Runnable()
				//	{
						//public void run() {Function.errorDialog(songDetails.getDfsId_M() + "\n\n" + Digest.getEncryptedId(songDetails.getDfsId_M()) + "\n\n" + songDetails.getMp3Url());}//playMp3(songDetails.getMp3Url());}
				//		public void run() {} //playMp3(songDetails.getMp3Url());}
				//	});
				//}
			//}
			//});
	}

	protected boolean onSavePrompt() {return true;}
}