package com.walkline.screen;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.Characters;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.AlbumDetails;
import com.walkline.music.inf.Albums;
import com.walkline.music.inf.Playlist;
import com.walkline.music.inf.SongDetails;
import com.walkline.music.inf.SongsList;
import com.walkline.util.Enumerations.RefreshActions;
import com.walkline.util.Enumerations.Toplists;
import com.walkline.util.Function;

public class RefreshContentsScreen extends PopupScreen
{
	Thread thread = null;
	MusicSDK _music;
	String _param;
	int _param_int;
	SongsList _songsList;
	SongDetails _songDetails;
	Albums _albumsList;
	AlbumDetails _albumDetails;
	Playlist _playList;

	public RefreshContentsScreen(MusicSDK music, String param, int action)
	{
		super(new VerticalFieldManager());

		_music = music;
		_param = param;

		add(new LabelField("Please wait....", Field.FIELD_HCENTER));

		switch (action)
		{
			case RefreshActions.SONGSLIST:
				thread = new Thread(new SongsListRunnable());
				break;
			case RefreshActions.SONGDETAILS:
				thread = new Thread(new SongDetailsRunnable());
				break;
			case RefreshActions.ALBUMSLIST:
				thread = new Thread(new AlbumsListRunnable());
				break;
			case RefreshActions.ALBUMDETAILS:
				thread = new Thread(new AlbumListRunnable());
				break;
		}

		thread.start();
	}

	public RefreshContentsScreen(MusicSDK music, int param, int action)
	{
		super(new VerticalFieldManager());

		_music = music;
		_param_int = param;

		add(new LabelField("Please wait....", Field.FIELD_HCENTER));

		switch (action)
		{
			case RefreshActions.PLAYLIST:
				thread = new Thread(new PlaylistRunnable());
				break;
		}

		thread.start();
	}

	class SongsListRunnable implements Runnable
	{
		public void run()
		{
			try {
				_songsList = _music.getSongsList(_param);

				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {onClose();}
				});
			} catch (MusicException e) {Function.errorDialog(e.toString());}
		}
	}

	class SongDetailsRunnable implements Runnable
	{
		public void run()
		{
			try {
				_songDetails = _music.getSongDetails(_param);

				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {onClose();}
				});
			} catch (MusicException e) {Function.errorDialog(e.toString());}
		}
	}

	class AlbumsListRunnable implements Runnable
	{
		public void run()
		{
			try {
				_albumsList = _music.getAlbumsList();

				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {onClose();}
				});
			} catch (MusicException e) {}
		}
	}

	class AlbumListRunnable implements Runnable
	{
		public void run()
		{
			try {
				_albumDetails = _music.getAlbumDetails(_param);

				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {onClose();}
				});
			} catch (MusicException e) {}
		}
	}

	class PlaylistRunnable implements Runnable
	{
		public void run()
		{
			try {
				_playList = _music.getPlaylist(Toplists.playlistIds[_param_int]);

				Application.getApplication().invokeLater(new Runnable()
				{
					public void run() {onClose();}
				});
			} catch (MusicException e) {}
		}
	}

	public SongsList getSongsList() {return _songsList;}

	public SongDetails getSongDetail() {return _songDetails;}

	public Albums getAlbumsList() {return _albumsList;}

	public AlbumDetails getAlbumDetails() {return _albumDetails;}

	public Playlist getPlaylist() {return _playList;}

	public boolean onClose()
	{
		if (thread != null)
		{
			try {
				thread.interrupt();
				thread = null;
			} catch (Exception e) {}
		}

		try {
			UiApplication.getUiApplication().popScreen(this);	
		} catch (Exception e) {}

		return true;
	}

	protected boolean keyChar(char key, int status, int time)
	{
		if (key == Characters.ESCAPE)
		{
			onClose();

			return true;
		}

		return super.keyChar(key, status, time);
	}
} 