package com.walkline.music.inf;

public interface SongDetails extends com.walkline.music.inf.Object
{
	public int getCode();

	public boolean getStarred();

	public int getPopularity();

	public int getStarredNum();

	public int getPlayedNum();

	public int getDayPlays();

	public int getHearTime();

	public String getName();

	public int getId();

	public int getDuration();

	public int getStatus();

	public int getPosition();

	public String getCommentThreadId();

	public int getScore();

	public String getCopyFrom();

	public String getRingtone();

	public int getCopyrightId();

	public int getMvId();

	public long getDfsId_B();

	public long getDfsId_L();

	public long getDfsId_M();

	public long getDfsId_H();

	public String getMp3Url();
}