package com.walkline.music.inf;

public interface Album
{
	public int getId();

	public String getName();

	public Artist getArtist();

	public int getPublishTime();

	public int getSize();

	public int getCopyrightId();

	public int getStatus();

	public Song[] getSongList();
}