package com.walkline.music.inf;

public interface Artist
{
	public int getId();

	public String getName();

	public String getPicUrl();

	public String getAlias();

	public int getAlbumSize();

	public int getPicId();
}