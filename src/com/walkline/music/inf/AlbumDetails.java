package com.walkline.music.inf;

public interface AlbumDetails
{
	public int getCode();

	public Album getAlbum();
}