package com.walkline.music.inf;

import java.util.Vector;

public interface Song
{
	public int getId();

	public String getName();

	public Vector getArtistsList();

	public Album getAlbum();

	public int getDuration();

	public int getCopyrightId();

	public int getStatus();

	public String getAlias();

	public int getMvId();
}