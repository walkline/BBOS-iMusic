package com.walkline.music.inf;

import java.util.Vector;

public interface SongsList extends com.walkline.music.inf.Object
{
	public int getSongCount();

	public int getCode();

	public Vector getSongsList();
}