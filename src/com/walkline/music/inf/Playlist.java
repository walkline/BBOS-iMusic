package com.walkline.music.inf;

public interface Playlist
{
	public int getCode();

	public Tracks[] getSongsList();
}