package com.walkline.music.inf;

public interface Albums
{
	public int getCode();

	public int getTotal();

	public Album[] getAlbums();
}