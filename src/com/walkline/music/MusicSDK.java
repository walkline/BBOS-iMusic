package com.walkline.music;

import java.util.Hashtable;

import javax.microedition.io.HttpConnection;

import net.rim.device.api.system.Clipboard;

import com.walkline.app.MusicAppConfig;
import com.walkline.music.dao.MusicAlbumDetails;
import com.walkline.music.dao.MusicAlbums;
import com.walkline.music.dao.MusicPlaylist;
import com.walkline.music.dao.MusicSongDetails;
import com.walkline.music.dao.MusicSongsList;
import com.walkline.music.inf.AlbumDetails;
import com.walkline.music.inf.Albums;
import com.walkline.music.inf.Playlist;
import com.walkline.music.inf.SongDetails;
import com.walkline.music.inf.SongsList;
import com.walkline.util.StringUtility;
import com.walkline.util.json.JSONObject;
import com.walkline.util.json.JSONTokener;
import com.walkline.util.network.HttpClient;
import com.walkline.util.network.MyConnectionFactory;

public class MusicSDK
{
	protected HttpClient _http;

	public static MusicSDK getInstance() {return new MusicSDK();}

	protected MusicSDK()
	{
		MyConnectionFactory cf = new MyConnectionFactory();

		_http = new HttpClient(cf);
	}

	private SongsList getSongsList(JSONObject jsonObject) throws MusicException {return new MusicSongsList(this, jsonObject);}
	public SongsList getSongsList(String keyword) throws MusicException {return getSongsList(keyword, null);}
	private SongsList getSongsList(final String keyword, final Object state)
	{
		SongsList result = null;
		Hashtable args = new Hashtable();

		args.put("s", keyword);
		args.put("type", "1");
		args.put("offset", "0");
		args.put("limit", "20");

		JSONObject jsonObject;
		try {
			jsonObject = doRequest(HttpConnection.POST, MusicAppConfig.querySongsListRequestURL, args);
			result = (jsonObject != null ? getSongsList(jsonObject) : null);
		} catch (Exception e) {}

		return result;
	}

	private SongDetails getSongDetails(JSONObject jsonObject) throws MusicException {return new MusicSongDetails(this, jsonObject);}
	public SongDetails getSongDetails(String id) throws MusicException {return getSongDetails(id, null);}
	private SongDetails getSongDetails(final String id, final Object state)
	{
		SongDetails result = null;

		JSONObject jsonObject;
		try {
			String api = StringUtility.replaceAll(MusicAppConfig.querySongDetailsRequestURL, "$ID$", id);
			jsonObject = doRequest(HttpConnection.GET, api, null);
			result = (jsonObject != null ?  getSongDetails(jsonObject) : null);
		} catch (Exception e) {}

		return result;
	}

	//private JSONObject getSongsDetails(JSONObject jsonObject) throws MusicException {return new MusicSongsList(this, jsonObject);}
	public JSONObject getSongsDetails(String ids) throws MusicException {return getSongsDetails(ids, null);}
	private JSONObject getSongsDetails(final String ids, final Object state)
	{
		JSONObject result = null;

		JSONObject jsonObject;
		try {
			String api = StringUtility.replace(MusicAppConfig.querySongsDetailsRequestURL, "$IDS$", ids);
			jsonObject = doRequest(HttpConnection.GET, api, null);
			result = (jsonObject != null ? jsonObject : null);
		} catch (Exception e) {}

		return result;
	}

	private Albums getAlbumsList(JSONObject jsonObject) throws MusicException {return new MusicAlbums(this, jsonObject);}
	//public Album getAlbumsList() throws MusicException {return getAlbumsList(null);}
	public Albums getAlbumsList() throws MusicException
	{
		Albums result = null;

		JSONObject jsonObject;
		try {
			jsonObject = doRequest(HttpConnection.GET, MusicAppConfig.queryNewAlbumRequestURL, null);
			result = (jsonObject != null ? getAlbumsList(jsonObject) : null);
		} catch (Exception e) {}

		return result;
	}

	private AlbumDetails getAlbumDetails(JSONObject jsonObject) throws MusicException {return new MusicAlbumDetails(this, jsonObject);}
	public AlbumDetails getAlbumDetails(String albumId) throws MusicException {return getAlbumDetails(albumId, null);}
	public AlbumDetails getAlbumDetails(final String albumId, final Object state) throws MusicException
	{
		AlbumDetails result = null;

		JSONObject jsonObject;
		try {
			String api = MusicAppConfig.queryAlbumDetailsRequestURL + albumId;
			jsonObject = doRequest(HttpConnection.GET, api, null);
			result = (jsonObject != null ? getAlbumDetails(jsonObject) : null);
		} catch (Exception e) {}

		return result;
	}

	private Playlist getPlaylist(JSONObject jsonObject) throws MusicException {return new MusicPlaylist(this, jsonObject);}
	public Playlist getPlaylist(String playlistId) throws MusicException {return getPlaylist(playlistId, null);}
	public Playlist getPlaylist(final String playlistId, final Object state) throws MusicException
	{
		Playlist result = null;

		JSONObject jsonObject;
		try {
			String api = MusicAppConfig.queryPlaylistRequestURL + playlistId;
			jsonObject = doRequest(HttpConnection.GET, api, null);
			result = (jsonObject != null ? getPlaylist(jsonObject) : null);
		} catch (Exception e) {}

		return result;
	}








//**************************************************************************************************
//
//             //              //      //    //      ////////////////            //      //        
//             //            //    //  //  //                    //          //  //  //  //        
// //////////  //          //////////  ////        //          //      //        //      //        
//     //    ////////////              //      //  //  //    //    //  //  ////////////  ////////  
//     //      //      //    ////////    ////////  //    //  //  //    //      ////    //    //    
//     //      //      //    //    //              //        //        //    //  ////    //  //    
//     //      //      //    ////////  //          //    //  //  //    //  //    //  //  //  //    
//     //      //      //    //    //  //    //    //  //    //    //  //      //        //  //    
//     ////////        //    ////////  //  //      //        //        //  //////////    //  //    
// ////      //        //    //    //  ////    //  //      ////        //    //    //      //      
//         //          //    //    //  //      //  //                  //      ////      //  //    
//       //        ////      //  ////    ////////  //////////////////////  ////    //  //      //  
//
//**************************************************************************************************
	private JSONObject doRequest(String methond, String api, Hashtable args) throws Exception
	{
		JSONObject result = null;
		StringBuffer responseBuffer = null;

		try {
			if (methond.equals(HttpConnection.POST))
			{
				responseBuffer = _http.doPost(api, args);	
			} else {
				responseBuffer = _http.doGet(api);
			}

			if ((responseBuffer == null) || (responseBuffer.length() <= 0))
			{
				result = null;
			} else {
				result = new JSONObject(new JSONTokener(new String(responseBuffer.toString().getBytes(), "utf-8")));
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		} catch (Throwable t) {
			throw new Exception(t.getMessage());
		}

		return result;
	}
}