package com.walkline.music;

public class MusicException extends Exception
{
	public MusicException() {super();}

	public MusicException(String message) {super(message);}
}