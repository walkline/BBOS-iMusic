package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Album;
import com.walkline.music.inf.Albums;
import com.walkline.util.Function;
import com.walkline.util.json.JSONArray;
import com.walkline.util.json.JSONException;
import com.walkline.util.json.JSONObject;

public class MusicAlbums extends MusicObject implements Albums
{
	private int _code = 0;
	private int _total = 0;
	private Album[] _albums_list;

	public MusicAlbums(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		JSONObject albumsObject = jsonObject;
		if (albumsObject != null)
		{
			_code = albumsObject.optInt("code");
			_total = albumsObject.optInt("total");

			JSONArray albumsArray = albumsObject.optJSONArray("albums");
			if (albumsArray != null)
			{
				JSONObject albumObject;
				_albums_list = new Album[albumsArray.length()];

				for (int i=0; i<albumsArray.length(); i++)
				{
					try {
						albumObject = (JSONObject) albumsArray.get(i);

						Album album = new MusicAlbum(music, albumObject);
						if (album != null) {_albums_list[i] = album;}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public int getCode() {return _code;}

	public int getTotal() {return _total;}

	public Album[] getAlbums() {return _albums_list;}
}