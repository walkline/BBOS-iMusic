package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.util.json.JSONObject;

public class MusicObject implements com.walkline.music.inf.Object
{
	protected MusicSDK music;
	protected JSONObject jsonObject;

	public MusicObject(MusicSDK pMusic, JSONObject pJsonObject) throws MusicException
	{
		if ((pMusic == null) || (pJsonObject == null)) {
			throw new MusicException("Unable to create WalkMusic MusicObject.");
		}

		music = pMusic;
		jsonObject = pJsonObject;
	}
}