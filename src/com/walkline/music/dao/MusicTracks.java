package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Album;
import com.walkline.music.inf.Artist;
import com.walkline.music.inf.Tracks;
import com.walkline.util.json.JSONArray;
import com.walkline.util.json.JSONException;
import com.walkline.util.json.JSONObject;

public class MusicTracks extends MusicObject implements Tracks
{
	private long _dfs_id_h = 0;
	private long _dfs_id_m = 0;
	private long _dfs_id_l = 0;
	private long _dfs_id_b = 0;
	private String _mp3_url = "";
	private Album _album;
	private Artist _artist;
	private String _name;
	private int _duration = 0;

	public MusicTracks(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		JSONObject songObject = jsonObject;
		if (songObject != null)
		{
			_name = songObject.optString("name");
			_duration = songObject.optInt("duration");

			JSONArray artistArray = songObject.optJSONArray("artists");
			if (artistArray != null)
			{
				try {
					_artist = new MusicArtist(music, (JSONObject) artistArray.get(0));
				} catch (JSONException e) {}
			}

			JSONObject albumObject = songObject.optJSONObject("album");
			if (albumObject != null) {_album = new MusicAlbum(music, albumObject);}

			JSONObject hMusic = songObject.optJSONObject("hMusic");
			if (hMusic != null) {_dfs_id_h = hMusic.optLong("dfsId");}

			JSONObject mMusic = songObject.optJSONObject("mMusic");
			if (mMusic != null) {_dfs_id_m = mMusic.optLong("dfsId");}

			JSONObject lMusic = songObject.optJSONObject("lMusic");
			if (lMusic != null) {_dfs_id_l = lMusic.optLong("dfsId");}

			JSONObject bMusic = songObject.optJSONObject("bMusic");
			if (bMusic != null) {_dfs_id_b = bMusic.optLong("dfsId");}

			_mp3_url = songObject.optString("mp3Url");	
		}
	}

	public boolean getStarred() {return false;}

	public int getPopularity() {return 0;}

	public int getStarredNum() {return 0;}

	public int getPlayedNum() {return 0;}

	public int getDayPlays() {return 0;}

	public int getHearTime() {return 0;}

	public String getName() {return _name;}

	public int getId() {return 0;}

	public int getDuration() {return _duration;}

	public int getStatus() {return 0;}

	public int getPosition() {return 0;}

	public String getCommentThreadId() {return null;}

	public int getScore() {return 0;}

	public String getCopyFrom() {return null;}

	public String getRingtone() {return null;}

	public int getCopyrightId() {return 0;}

	public int getMvId() {return 0;}

	public String getMp3Url() {return _mp3_url;}

	public long getDfsId_B() {return _dfs_id_b;}

	public long getDfsId_L() {return _dfs_id_l;}

	public long getDfsId_M() {return _dfs_id_m;}

	public long getDfsId_H() {return _dfs_id_h;}

	public Artist getArtist() {return _artist;}

	public Album getAlbum() {return _album;}
}