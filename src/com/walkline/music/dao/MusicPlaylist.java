package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Playlist;
import com.walkline.music.inf.Tracks;
import com.walkline.util.Function;
import com.walkline.util.json.JSONArray;
import com.walkline.util.json.JSONException;
import com.walkline.util.json.JSONObject;

public class MusicPlaylist extends MusicObject implements Playlist
{
	private int _code = 0;
	private Tracks[] _songs_list;

	public MusicPlaylist(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		if (jsonObject != null)
		{
			_code = jsonObject.optInt("code");

			JSONObject resultObject = jsonObject.optJSONObject("result");
			if (resultObject != null)
			{
				JSONArray trackArray = resultObject.optJSONArray("tracks");
				if (trackArray != null)
				{
					_songs_list = new Tracks[trackArray.length()];
					JSONObject songObject;

					for (int i=0; i<trackArray.length(); i++)
					{
						try {
							songObject = (JSONObject) trackArray.get(i);

							Tracks song = new MusicTracks(music, songObject);
							if (song != null) {_songs_list[i] = song;}
						} catch (JSONException e) {Function.errorDialog(e.toString());}
					}
				}
			}
		}
	}

	public int getCode() {return _code;}

	public Tracks[] getSongsList() {return _songs_list;}
}