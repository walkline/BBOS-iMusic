package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Artist;
import com.walkline.util.json.JSONObject;

public class MusicArtist extends MusicObject implements Artist
{
	private int _id = 0;
	private String _name = "";
	private String _pic_url = "";
	private String _alias = "";
	private int _album_size = 0;
	private int _pic_id = 0;

	public MusicArtist(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		JSONObject artistObject = jsonObject;
		if (artistObject != null)
		{
			_id = artistObject.optInt("id");
			_name = artistObject.optString("name");
			_pic_url = artistObject.optString("picUrl");
			_alias = artistObject.optString("alias");
			_album_size = artistObject.optInt("albumSize");
			_pic_id = artistObject.optInt("picId");
		}
	}

	public int getId() {return _id;}

	public String getName() {return _name;}

	public String getPicUrl() {return _pic_url;}

	public String getAlias() {return _alias;}

	public int getAlbumSize() {return _album_size;}

	public int getPicId() {return _pic_id;}
}