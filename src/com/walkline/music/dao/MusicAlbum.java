package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Album;
import com.walkline.music.inf.Artist;
import com.walkline.music.inf.Song;
import com.walkline.util.Function;
import com.walkline.util.json.JSONArray;
import com.walkline.util.json.JSONException;
import com.walkline.util.json.JSONObject;

public class MusicAlbum extends MusicObject implements Album
{
	private int _id = 0;
	private String _name = "";
	private Artist _artist;
	private int _publish_time = 0;
	private int _size = 0;
	private int _copyright_id = 0;
	private int _status = 0;
	private Song[] _songs_list;

	public MusicAlbum(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		JSONObject albumObject = jsonObject;
		if (albumObject != null)
		{
			_id = albumObject.optInt("id");
			_name = albumObject.optString("name");
			_publish_time = albumObject.optInt("publishTime");
			_size = albumObject.optInt("size");
			_copyright_id = albumObject.optInt("copyrightId");
			_status = albumObject.optInt("status");

			JSONObject artistObject = albumObject.optJSONObject("artist");
			if (artistObject != null) {_artist = new MusicArtist(music, artistObject);}

			JSONArray songsArray = albumObject.optJSONArray("songs");
			if (songsArray != null)
			{
				JSONObject songObject;
				_songs_list = new Song[songsArray.length()];

				for (int i=0; i<songsArray.length(); i++)
				{
					try {
						songObject = (JSONObject) songsArray.get(i);

						Song song = new MusicSong(music, songObject);
						if (song != null) {_songs_list[i] = song;}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}
		}
	}

	public int getId() {return _id;}

	public String getName() {return _name;}

	public Artist getArtist() {return _artist;}

	public int getPublishTime() {return _publish_time;}

	public int getSize() {return _size;}

	public int getCopyrightId() {return _copyright_id;}

	public int getStatus() {return _status;}

	public Song[] getSongList() {return _songs_list;}
}