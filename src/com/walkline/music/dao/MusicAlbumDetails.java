package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Album;
import com.walkline.music.inf.AlbumDetails;
import com.walkline.util.json.JSONObject;

public class MusicAlbumDetails extends MusicObject implements AlbumDetails
{
	private int _code = 0;
	private Album _album;

	public MusicAlbumDetails(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		if (jsonObject != null)
		{
			_code = jsonObject.optInt("code");

			JSONObject albumObject = jsonObject.optJSONObject("album");
			if (albumObject != null)
			{
				_album = new MusicAlbum(music, albumObject);
			}
		}
	}

	public int getCode() {return _code;}

	public Album getAlbum() {return _album;}
}