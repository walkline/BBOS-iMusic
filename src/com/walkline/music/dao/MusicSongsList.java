package com.walkline.music.dao;

import java.util.Vector;
import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Song;
import com.walkline.music.inf.SongsList;
import com.walkline.util.Function;
import com.walkline.util.json.JSONArray;
import com.walkline.util.json.JSONException;
import com.walkline.util.json.JSONObject;

public class MusicSongsList extends MusicObject implements SongsList
{
	private int _song_count = 0;
	private int _code = 0;
	private Vector _songs_list = new Vector();

	public MusicSongsList(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		JSONObject result = jsonObject;
		if (result != null)
		{
			JSONObject resultObject = result.optJSONObject("result");
			if (resultObject != null)
			{
				_song_count = resultObject.optInt("songCount");
				_code = resultObject.optInt("code");

				JSONArray songsArray = resultObject.optJSONArray("songs");
				if (songsArray != null)
				{
					JSONObject songObject;
					for (int i=0; i<songsArray.length(); i++)
					{
						try {
							songObject = (JSONObject) songsArray.get(i);

							Song song = new MusicSong(music, songObject);
							if (song != null) {_songs_list.addElement(song);}
						} catch (JSONException e) {Function.errorDialog(e.toString());}
					}
				}
			}
		}
	}

	public int getSongCount() {return _song_count;}

	public int getCode() {return _code;}

	public Vector getSongsList() {return _songs_list;}
}