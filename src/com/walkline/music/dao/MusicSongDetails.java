package com.walkline.music.dao;

import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.SongDetails;
import com.walkline.util.json.JSONArray;
import com.walkline.util.json.JSONException;
import com.walkline.util.json.JSONObject;

public class MusicSongDetails extends MusicObject implements SongDetails
{
	private int _code = 0;
	private long _dfs_id_h = 0;
	private long _dfs_id_m = 0;
	private long _dfs_id_l = 0;
	private long _dfs_id_b = 0;
	private String _mp3_url = "";

	public MusicSongDetails(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		JSONObject songs = jsonObject;
		if (songs != null)
		{
			_code = songs.optInt("code");

			JSONArray songsArray = songs.optJSONArray("songs");
			if (songsArray != null)
			{
				JSONObject songObject;
				try {
					songObject = (JSONObject) songsArray.get(0);
					if (songObject != null)
					{
						JSONObject hMusic = songObject.optJSONObject("hMusic");
						if (hMusic != null) {_dfs_id_h = hMusic.optLong("dfsId");}

						JSONObject mMusic = songObject.optJSONObject("mMusic");
						if (mMusic != null) {_dfs_id_m = mMusic.optLong("dfsId");}

						JSONObject lMusic = songObject.optJSONObject("lMusic");
						if (lMusic != null) {_dfs_id_l = lMusic.optLong("dfsId");}

						JSONObject bMusic = songObject.optJSONObject("bMusic");
						if (bMusic != null) {_dfs_id_b = bMusic.optLong("dfsId");}

						_mp3_url = songObject.optString("mp3Url");	
					}
				} catch (JSONException e) {}
			}
		}
	}

	public int getCode() {return _code;}

	public boolean getStarred() {return false;}

	public int getPopularity() {return 0;}

	public int getStarredNum() {return 0;}

	public int getPlayedNum() {return 0;}

	public int getDayPlays() {return 0;}

	public int getHearTime() {return 0;}

	public String getName() {return null;}

	public int getId() {return 0;}

	public int getDuration() {return 0;}

	public int getStatus() {return 0;}

	public int getPosition() {return 0;}

	public String getCommentThreadId() {return null;}

	public int getScore() {return 0;}

	public String getCopyFrom() {return null;}

	public String getRingtone() {return null;}

	public int getCopyrightId() {return 0;}

	public int getMvId() {return 0;}

	public String getMp3Url() {return _mp3_url;}

	public long getDfsId_B() {return _dfs_id_b;}

	public long getDfsId_L() {return _dfs_id_l;}

	public long getDfsId_M() {return _dfs_id_m;}

	public long getDfsId_H() {return _dfs_id_h;}
}