package com.walkline.music.dao;

import java.util.Vector;
import com.walkline.music.MusicException;
import com.walkline.music.MusicSDK;
import com.walkline.music.inf.Album;
import com.walkline.music.inf.Artist;
import com.walkline.music.inf.Song;
import com.walkline.util.Function;
import com.walkline.util.json.JSONArray;
import com.walkline.util.json.JSONException;
import com.walkline.util.json.JSONObject;

public class MusicSong extends MusicObject implements Song
{
	private int _id = 0;
	private String _name = "";
	private Album _album;
	private int _duration = 0;
	private int _copyright_id = 0;
	private int _status = 0;
	private String _alias = "";
	private int _mv_id = 0;
	private Vector _artists_list = new Vector();

	public MusicSong(MusicSDK music, JSONObject jsonObject) throws MusicException
	{
		super(music, jsonObject);

		JSONObject songObject = jsonObject;
		if (songObject != null)
		{
			_id = songObject.optInt("id");
			_name = songObject.optString("name");
			_duration = songObject.optInt("duration");
			_copyright_id = songObject.optInt("copyrightId");
			_status = songObject.optInt("status");
			_alias = songObject.optString("alias");
			_mv_id = songObject.optInt("mvid");

			JSONArray artistsArray = songObject.optJSONArray("artists");
			if (artistsArray != null)
			{
				JSONObject artistObject;
				for (int i=0; i<artistsArray.length(); i++)
				{
					try {
						artistObject = (JSONObject) artistsArray.get(i);

						Artist artist = new MusicArtist(music, artistObject);
						if (artist != null) {_artists_list.addElement(artist);}
					} catch (JSONException e) {Function.errorDialog(e.toString());}
				}
			}

			JSONObject albumObject = songObject.optJSONObject("album");
			if (albumObject != null) {_album = new MusicAlbum(music, albumObject);}
		}
	}

	public int getId() {return _id;}

	public String getName() {return _name;}

	public Vector getArtistsList() {return _artists_list;}

	public Album getAlbum() {return _album;}

	public int getDuration() {return _duration;}

	public int getCopyrightId() {return _copyright_id;}

	public int getStatus() {return _status;}

	public String getAlias() {return _alias;}

	public int getMvId() {return _mv_id;}
}