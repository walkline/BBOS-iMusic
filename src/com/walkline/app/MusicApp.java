package com.walkline.app;

import com.walkline.screen.MusicScreen;
import net.rim.device.api.ui.UiApplication;

public class MusicApp extends UiApplication
{
    public static void main(String[] args)
    {
        MusicApp theApp = new MusicApp();       
        theApp.enterEventDispatcher();
    }
    
    public MusicApp() {pushScreen(new MusicScreen());}    
}