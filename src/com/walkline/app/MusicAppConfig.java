package com.walkline.app;

import net.rim.device.api.io.transport.TransportInfo;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;

public class MusicAppConfig
{
	public static final String APP_TITLE = "iMusic";
	public static final String UNDERLINE = "\u0332";
	public static final String BBW_APPID = "58645271";

	public static final String querySongsListRequestURL = "http://music.163.com/api/search/get/";
	public static final String queryDownloadRequestURL = "http://m1.music.126.net/";
	public static final String querySongDetailsRequestURL = "http://music.163.com/api/song/detail/?id=$ID$&ids=[$ID$]&csrf_token=";
	public static final String querySongsDetailsRequestURL = "http://music.163.com/api/song/detail/?ids=[$IDS$]&csrf_token=";
	public static final String queryNewAlbumRequestURL = "http://music.163.com/api/album/new?area=all&offset=0&total=true&limit=20";
	public static final String queryAlbumDetailsRequestURL = "http://music.163.com/api/album/";
	public static final String queryPlaylistRequestURL = "http://music.163.com/api/playlist/detail?id=";

	public static final Background bgColor_Gradient=BackgroundFactory.createLinearGradientBackground(Color.GRAY, Color.GRAY, Color.BLACK, Color.BLACK);

	//public static final Border border_popup_Transparent=BorderFactory.createRoundedBorder(new XYEdges(16,16,16,16), Color.BLACK, 200, Border.STYLE_FILLED);
	public static final Background bg_popup_Transparent=BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 200);

	public static int[] preferredTransportTypes = {
													TransportInfo.TRANSPORT_TCP_WIFI,
													TransportInfo.TRANSPORT_BIS_B,
													TransportInfo.TRANSPORT_TCP_CELLULAR,
													TransportInfo.TRANSPORT_WAP2
												  };
	public static int[] disallowedTransportTypes = {
													TransportInfo.TRANSPORT_MDS,
													TransportInfo.TRANSPORT_WAP
												   };

	public static final Font FONT_SONG_TITLE = Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt) + 1, Ui.UNITS_pt);
	public static final Font FONT_SONG_ALBUM = Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt), Ui.UNITS_pt);
	public static final Font FONT_MAIN_TITLE = Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt) + 1, Ui.UNITS_pt);

	public static final Font FONT_ABOUT_HEADLINE = Font.getDefault().derive(Font.BOLD | Font.ITALIC, Font.getDefault().getHeight(Ui.UNITS_pt), Ui.UNITS_pt);
	public static final Font FONT_ABOUT_SMALL = Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1, Ui.UNITS_pt);
	public static final Font FONT_ABOUT_LARGE = Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+1, Ui.UNITS_pt);

	//SKU: 0x823252ddc046c845L (zhihu_daily_for_you_written_by_walkline_wang)
}