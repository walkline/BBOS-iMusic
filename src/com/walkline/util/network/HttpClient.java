package com.walkline.util.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.microedition.io.HttpConnection;

import net.rim.blackberry.api.browser.PostData;
import net.rim.blackberry.api.browser.URLEncodedPostData;
import net.rim.device.api.compress.GZIPInputStream;
import net.rim.device.api.io.http.HttpProtocolConstants;
import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.io.transport.ConnectionFactory;

import com.walkline.util.Function;

public class HttpClient
{
	protected ConnectionFactory cf;

	public HttpClient(ConnectionFactory pcf) {cf = pcf;}

	public StringBuffer doGet(String url) throws Exception
	{
		HttpConnection conn = null;
		StringBuffer buffer = new StringBuffer();

		try {
			if ((url == null) || url.equalsIgnoreCase("") || (cf == null)) {Function.errorDialog("null"); return null;}

			ConnectionDescriptor connd = cf.getConnection(url);
			conn = (HttpConnection) connd.getConnection();

			conn.setRequestProperty(HttpProtocolConstants.HEADER_ACCEPT_ENCODING, "gzip, deflate");
			conn.setRequestProperty(HttpProtocolConstants.HEADER_CONNECTION, HttpProtocolConstants.HEADER_KEEP_ALIVE);
			conn.setRequestProperty(HttpProtocolConstants.HEADER_REFERER, "http://music.163.com");

			int resCode = conn.getResponseCode();

			switch (resCode)
			{
				case HttpConnection.HTTP_OK: 
				{
					InputStream inputStream;

					if (conn.getEncoding() != null)
					{
						if (conn.getEncoding().equalsIgnoreCase("gzip"))
						{
							inputStream = conn.openInputStream();
							GZIPInputStream gzipInputStream = new GZIPInputStream(inputStream);

							int c;

							while ((c = gzipInputStream.read()) != -1) {buffer.append((char) c);}

							gzipInputStream.close();
							inputStream.close();

							break;
						}
					} else {
						inputStream = conn.openInputStream();

						int c;

						while ((c = inputStream.read()) != -1) {buffer.append((char) c);}

						inputStream.close();

						break;
					}
				}
			}
		} catch (Exception e) {throw e;}
		  finally {if (conn != null) {try {conn.close(); conn = null;} catch (IOException e) {Function.errorDialog(e.toString());}}}

		return buffer;
	}

	public StringBuffer doPost(String url, Hashtable data) throws Exception
	{
		URLEncodedPostData encoder = new URLEncodedPostData("UTF-8", false);
		Enumeration keysEnum = data.keys();

		while (keysEnum.hasMoreElements()) {
			String key = (String) keysEnum.nextElement();
			String val = (String) data.get(key);
			encoder.append(key, val);
		}

		return doPost(url, encoder);
	}

	public StringBuffer doPost(String url, PostData postData) throws Exception
	{
		HttpConnection conn = null;
		OutputStream os = null;
		StringBuffer buffer = new StringBuffer();

		try {
			if ((url == null) || url.equalsIgnoreCase("") || (cf == null)) {return null;}

			ConnectionDescriptor connd = cf.getConnection(url);
			conn = (HttpConnection) connd.getConnection();
			conn.setRequestProperty(HttpProtocolConstants.HEADER_REFERER, "http://music.163.com");
			conn.setRequestProperty(HttpProtocolConstants.HEADER_COOKIE, "appver=2.0.2;");

			if (conn != null)
			{
				try
				{
					if (postData != null)
					{
						conn.setRequestMethod(HttpConnection.POST);
						conn.setRequestProperty(HttpProtocolConstants.HEADER_CONTENT_TYPE, postData.getContentType());
						conn.setRequestProperty(HttpProtocolConstants.HEADER_CONTENT_LENGTH, String.valueOf(postData.size()));

						os = conn.openOutputStream();
						os.write(postData.getBytes());
					} else {
						conn.setRequestMethod(HttpConnection.GET);
					}
				} catch (Throwable t) {}

				int resCode = conn.getResponseCode();

				switch (resCode)
				{
					case HttpConnection.HTTP_OK:
					{
						InputStream inputStream = conn.openInputStream();
						int c;

						while ((c = inputStream.read()) != -1) {
							buffer.append((char) c);
						}

						inputStream.close();
						break;
					}
				}
			}
		} catch (Throwable t) {
		} finally {
			if (os != null) {try {os.close(); os = null;} catch (IOException e) {}}
			if (conn != null) {try {conn.close(); conn = null;} catch (IOException e) {}}
		}

		return buffer;
	}
}